#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;94m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;94m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


###########################
##                       ##
##  SET UP BUILDING ENV  ##
##                       ##
###########################

. /etc/os-release
case $ID in
debian)
    case $VERSION_CODENAME in
    bullseye|bookworm)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
alpine)
    CODENAME=static
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac
export CODENAME

src_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
dist_dir=$CI_PROJECT_DIR/dist/$CODENAME

# Prepare `file` command
if ! command -v file >/dev/null; then
    case $CODENAME in
    bullseye|bookworm)
        apt-get update
        apt-get install --no-install-recommends --yes file
        ;;
    static)
        apk --no-cache add file
        ;;
    *)
        echo "ERROR: CODENAME not found: $CODENAME"
        exit 1
        ;;
    esac
fi

# Prepare `envsubst` command
if ! command -v envsubst >/dev/null; then
    case $CODENAME in
    bullseye|bookworm)
        apt-get update
        apt-get install --no-install-recommends --yes gettext
        ;;
    static)
        apk --no-cache add gettext
        ;;
    *)
        echo "ERROR: CODENAME not found: $CODENAME"
        exit 1
        ;;
    esac
fi

# Prepare `nfpm` command
if ! command -v nfpm >/dev/null; then
    case $CODENAME in
    bullseye|bookworm) apt-get install --yes wget ;;
    static)  ;;
    *)
        echo "ERROR: CODENAME not found: $CODENAME"
        exit 1
        ;;
    esac
    wget --no-verbose -O- https://xdeb.gitlab.io/nfpm/nfpm-static_amd64.tar.gz | tar -C /usr/local/bin -xzf-
fi
nfpm --version

cd $src_dir


###############
##           ##
##  DO TEST  ##
##           ##
###############

# # FIXME: do a real TEST.
# go fmt $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go vet $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"
# go test -race $(go list ./... | grep -v /vendor/) || echo "******** TEST FAILED ********"


################
##            ##
##  DO BUILD  ##
##            ##
################

# Initial var `ldflags`
case $CODENAME in
bullseye|bookworm)
    test 1 -eq $(go env CGO_ENABLED)  # ensure CGO_ENABLED=1
    ldflags='-s -w'
    ;;
static)
    test 0 -eq $(go env CGO_ENABLED)  # ensure CGO_ENABLED=0
    ldflags='-s -w -extldflags="-static"'
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

# Build binary output
ldflags="$ldflags
    -X 'github.com/dundee/gdu/v5/build.Version=${PKG_VERSION}'
    -X 'github.com/dundee/gdu/v5/build.Time=$(date -u +%F\ %T\ %Z)'"
GOARCH=amd64 go build -o "$dist_dir/amd64/gdu" -ldflags="$ldflags" -trimpath ./cmd/gdu
GOARCH=386   go build -o "$dist_dir/386/gdu"   -ldflags="$ldflags" -trimpath ./cmd/gdu

case $CODENAME in
bullseye|bookworm)
    ldd $dist_dir/amd64/gdu
    ! ldd $dist_dir/386/gdu
    ;;
static)
    ! ldd $dist_dir/amd64/gdu
    ! ldd $dist_dir/386/gdu
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac
file $dist_dir/{amd64,386}/gdu


##################
##              ##
##  DO PACKING  ##
##              ##
##################

cd "$CI_PROJECT_DIR"

# import the sign key
test -n "$PRIVATE_SIGKEY"  # ensure $PRIVATE_SIGKEY is not empty
echo "$PRIVATE_SIGKEY" | base64 -d > key.gpg

mkdir -p $dist_dir/../noarch
cp -v $src_dir/README.md  $dist_dir/../noarch/README.md
cp -v $src_dir/LICENSE.md $dist_dir/../noarch/LICENSE.md
cp -v $src_dir/gdu.1      $dist_dir/../noarch/gdu.1 && gzip -9v $dist_dir/../noarch/gdu.1

mkdir -p $dist_dir
case $CODENAME in
bullseye)
    PKG_ARCH=amd64 TARGET_CODENAME=bullseye envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bullseye envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=focal envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=focal envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    ;;
bookworm)
    PKG_ARCH=amd64 TARGET_CODENAME=bookworm envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=bookworm envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=jammy envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=jammy envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin

    PKG_ARCH=amd64 TARGET_CODENAME=noble envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   TARGET_CODENAME=noble envsubst < nfpm.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    ;;
static)
    PKG_ARCH=amd64 envsubst < nfpm-static.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   envsubst < nfpm-static.yaml | nfpm pkg -p deb -t $dist_dir/.. -f /dev/stdin

    PKG_ARCH=amd64 envsubst < nfpm-apk.yaml | nfpm pkg -p apk -t $dist_dir/.. -f /dev/stdin
    PKG_ARCH=386   envsubst < nfpm-apk.yaml | nfpm pkg -p apk -t $dist_dir/.. -f /dev/stdin
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

# Check outputs
ls -lAFR --color $CI_PROJECT_DIR/dist/ $CI_PROJECT_DIR/dist/*/
